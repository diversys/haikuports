SUMMARY="A simple foundation for reading DVD video disks"
DESCRIPTION="
libdvdread provides a simple foundation for reading DVD video disks. It \
provides the functionality that is required to access many DVDs. libdvdread \
parses IFO files, reads NAV-blocks, and performs CSS authentication and \
descrambling (if an external libdvdcss library is installed).
"
HOMEPAGE="http://dvdnav.mplayerhq.hu"
LICENSE="GNU GPL v2"
COPYRIGHT="1998-1999 Eric Smith
	2000-2002 Björn Englund
	2000-2003 Håkan Hjort, et al."
SRC_URI="http://downloads.videolan.org/pub/videolan/libdvdread/$portVersion/libdvdread-$portVersion.tar.bz2"
CHECKSUM_SHA256="66fb1a3a42aa0c56b02547f69c7eb0438c5beeaf21aee2ae2c6aa23ea8305f14"
REVISION="1"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 ?x86"

PATCHES="libdvdread-$portVersion.patchset"

PROVIDES="
	libdvdread$secondaryArchSuffix = $portVersion
	lib:libdvdread$secondaryArchSuffix = 4.1.2 compat >= 4
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libdvdcss$secondaryArchSuffix
	"

BUILD_REQUIRES="
	devel:libdvdcss$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:libtoolize
	cmd:aclocal
	cmd:autoconf
	cmd:autoreconf
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure --enable-shared --enable-static \
		--disable-maintainer-mode
	touch ChangeLog
	make $jobArgs
}

INSTALL()
{
	make install

	# prepare develop/lib
	prepareInstalledDevelLibs libdvdread
	fixPkgconfig

	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libdvdread${secondaryArchSuffix}_devel = $portVersion
	devel:libdvdread$secondaryArchSuffix = 4.1.2 compat >= 4
	"
REQUIRES_devel="
	libdvdread$secondaryArchSuffix == $portVersion base
	"
